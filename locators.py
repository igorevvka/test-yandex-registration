# -*- coding: utf-8 -*-

from selenium.webdriver.common.by import By


class Locators(object):
    """Посик форм регистрации"""
    firstname = (By.ID, 'firstname')
    lastname = (By.ID, 'lastname')
    login = (By.ID, 'login')
    password = (By.ID, 'password')
    password_confirm = (By.ID, 'password_confirm')
    no_phone = (
        By.XPATH,
        '//form[@class="registration__form registration__form_desktop"]'
        '//div[@class="phone__confirm-wrapper"]//span[@role="button"]',
    )
    question_list = (
        By.XPATH,
        '//div[@class="user-question-wrapper"]'
        '//span[@data-t="user-question-all"]',
    )

    question_list_select = (
        By.XPATH,
        '//div[@class="menu menu_size_m menu_width_max menu_theme_normal '
        'menu_view_classic menu_type_radio select2__menu"]//div[2]',
    )
    question_list_answer = (By.ID, 'hint_answer')
    captcha = (
        By.XPATH,
        '//div [@class="captcha-wrapper"]//div[@class="error-message"]',
    )

    button_registration = (
        By.XPATH,
        '//div[@class ="form__submit"]'
        '//button[@class="control button2 button2_view_classic'
        ' button2_size_l button2_theme_action button2_width_max'
        ' button2_type_submit js-submit"]',
    )
