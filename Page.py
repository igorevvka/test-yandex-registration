# -*- coding: utf-8 -*-

from locators import Locators
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
import json


class RegPage(object):
    """Действия выполняемые на странице."""

    def __init__(self, driver, data_path):
        self.driver = driver

        with open(data_path, 'r', encoding='utf-8') as user_information:
            self.user_data = json.load(user_information)

    def click_go_button(self):
        """Нажатие клавиши регистрации."""
        reg_button = self.driver.find_element(*Locators.button_registration)
        reg_button.click()

    def set_values(self):
        """Заполнение полей ввода."""
        self.driver.find_element(*Locators.firstname).send_keys(
            self.user_data['name'],
        )
        self.driver.find_element(*Locators.lastname).send_keys(
            self.user_data['lastname'],
        )
        self.driver.find_element(*Locators.login).send_keys(
            self.user_data['login'],
        )
        self.driver.find_element(*Locators.password).send_keys(
            self.user_data['password'],
        )
        self.driver.find_element(*Locators.password_confirm).send_keys(
            self.user_data['password'],
        )

    def wait_captha(self):
        """Ожидание загрузки капчи."""
        wait = WebDriverWait(self.driver, 5)
        wait.until(
            expected_conditions.element_to_be_clickable(Locators.captcha),
        )

    def phone(self):
        """Нажатие на лик текс У меня нет телефона."""
        self.driver.find_element(*Locators.no_phone).click()
        wait = WebDriverWait(self.driver, 5)
        wait.until(
            expected_conditions.element_to_be_clickable(Locators.question_list),
        )

    def security_question(self):
        """Выбор серетного вопроса и ответ на него."""
        self.driver.find_element(*Locators.question_list).click()

        self.driver.find_element(*Locators.question_list_select).click()

        self.driver.find_element(*Locators.question_list_answer).send_keys(
            self.user_data['question_list_answer'],
        )

    def serch_text_title(self):
        """Проверка тайтла на правельный переход."""
        assert 'Регистрация' in self.driver.title

    def search_error_text(self):
        """Проверка наличия ошибки."""
        error_message = self.driver.find_elements(*Locators.captcha)

        if error_message:
            error_message = error_message[0]
            assert error_message.text == 'Необходимо ввести символы'
