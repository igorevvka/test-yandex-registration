# -*- coding: utf-8 -*-

import unittest
from Page import RegPage
from selenium import webdriver


class TestRegistration(unittest.TestCase):
    """Проверка формы регистрации нового пользователя Яндекс почты."""

    def setUp(self):
        """Команды выполняемые перед началом тестирования."""
        self.driver = webdriver.Chrome()
        self.driver.get('https://passport.yandex.ru/registration/')

    def test_registration(self):
        """Проверка формы ввода Капчи на пустое значени при регистрации."""
        reg_page = RegPage(self.driver, 'RegistrationForm.json')
        reg_page.serch_text_title()
        reg_page.set_values()
        reg_page.phone()
        reg_page.security_question()
        reg_page.click_go_button()
        reg_page.wait_captha()
        reg_page.search_error_text()


if __name__ == '__main__':
    unittest.main()
